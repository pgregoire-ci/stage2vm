#
# usage: $0 os release [variant]
#
set -eu

PROGBASE=$(d=$(dirname -- "${0}"); cd "${d}" && pwd)
PROGNAME=$(basename -- "${0}")

OS="${1}"
RELEASE="${2}"
VARIANT="${3:-default}"


DEBIAN_FRONTEND=noninteractive
export DEBIAN_FRONTEND

apk add curl cryptsetup dosfstools e2fsprogs e2fsprogs-extra qemu-img sgdisk

qemu-img create -f qcow2 disk.qcow2 10G

modprobe nbd max_part=63
qemu-nbd -c /dev/nbd0 disk.qcow2
cleanup() {
	cd
	mount | grep /mnt | cut -d' ' -f3 | sort -r | xargs -n1 umount
	[ ! -e /dev/mapper/vmluks ] || cryptsetup close vmluks
	qemu-nbd -d /dev/nbd0
}
trap cleanup EXIT INT QUIT TERM


sgdisk --zap-all /dev/nbd0
# partitioning: efi, /boot, luks
sgdisk -n1:0:+128M -t1:EF00 /dev/nbd0
sgdisk -n2:0:+1G /dev/nbd0
sgdisk -n3:0:0 /dev/nbd0

ROOT=/dev/nbd0p3
if [ Xluks = X"${VARIANT}" ]; then
	echo root | cryptsetup -q luksFormat /dev/nbd0p3
	echo root | cryptsetup open /dev/nbd0p3 vmluks
	ROOT=/dev/mapper/vmluks
fi

# file systems
mkfs.vfat -F32 /dev/nbd0p1
mkfs.ext4 -F /dev/nbd0p2
mkfs.ext4 -F "${ROOT}"

tune2fs -O ^metadata_csum_seed /dev/nbd0p2
tune2fs -O ^metadata_csum_seed "${ROOT}"

mount "${ROOT}" /mnt
mkdir /mnt/boot
mount /dev/nbd0p2 /mnt/boot
mkdir /mnt/boot/efi
mount /dev/nbd0p1 /mnt/boot/efi

gzip -cd stage2.tar.gz | (cd /mnt && tar -f- -xp)

# https://bugs.debian.org/cgi-bin/bugreport.cgi?bug=925146
[ -d /mnt/run/udev ] || mkdir /mnt/run/udev
mount --rbind /run/udev /mnt/run/udev
mount --make-rslave /mnt/run/udev

cp "${PROGBASE}"/lxd* /mnt/lib/systemd/system
chmod 0644 /mnt/lib/systemd/system/lxd-agent.service
chmod 0644 /mnt/lib/systemd/system/lxd-agent-9p.service
chown 0:0 /mnt/lib/systemd/system/lxd-agent.service
chown 0:0 /mnt/lib/systemd/system/lxd-agent-9p.service

cd

sh /mnt/root/enter.sh <<__EOF__
#!/bin/sh
set -eux

DEBIAN_FRONTEND=noninteractive
export DEBIAN_FRONTEND

systemctl enable lxd-agent-9p.service
systemctl enable lxd-agent.service

echo nameserver 9.9.9.9 >/etc/resolv.conf
apt-get update
apt-get -y upgrade
apt-get -y dist-upgrade

cat>>/etc/fstab<<__EOF2__
UUID=\$(blkid -ovalue -sUUID ${ROOT}) /         ext4 defaults,noatime 0 1
UUID=\$(blkid -ovalue -sUUID /dev/nbd0p2) /boot     ext4 defaults,noatime 0 2
UUID=\$(blkid -ovalue -sUUID /dev/nbd0p1) /boot/efi vfat defaults,noatime 0 2

none /tmp tmpfs nodev,noexec,nosuid,size=1G,mode=1777 0 0
__EOF2__

if [ Xluks = X"${VARIANT}" ]; then
	echo vmluks UUID=\$(blkid -ovalue -sUUID /dev/nbd0p3) none luks >>/etc/crypttab
	apt-get install -y cryptsetup-initramfs
fi


apt-get -y install cryptsetup grub-efi-amd64 shim-signed

cat>>/etc/default/grub<<__EOF2__
GRUB_CMDLINE_LINUX_DEFAULT="\\\${GRUB_CMDLINE_LINUX_DEFAULT} console=tty1 console=ttyS0"
GRUB_TERMINAL=console
__EOF2__

update-initramfs -u
grub-install --target=x86_64-efi --efi-directory=/boot/efi --bootloader-id="${OS}" --recheck --no-floppy
update-grub

# from lxd project
# This will create EFI/BOOT
grub-install --uefi-secure-boot --target=x86_64-efi --no-nvram --removable
# This will create EFI/debian
grub-install --uefi-secure-boot --target=x86_64-efi --no-nvram
update-grub

rm -f /etc/machine-id /var/lib/dbus/machine-id

echo root:root | chpasswd
chmod 0755 /

echo done
__EOF__
